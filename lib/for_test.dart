import 'dart:math';

List<String> tokenizerString(String str) {
  String temp = '';
  List<String> list = [];
  for (var i = 0; i <= str.length; i++) {
    if (i == str.length && !str[str.length - 1].contains(' ') ||
        str[i] == ' ') {
      if (temp.isNotEmpty) {
        list.add(temp);
        temp = '';
      }
    } else {
      temp += str[i];
    }
  }
  return list;
}

List<dynamic> infixToPostfix(List<String> token) {
  List<String> operations = ['+', '-', '*', '/', '^'];
  List<String> operators = [];
  List<String> postfix = [];
  Map<String, int> procedence = {
    '+': 0,
    '-': 0,
    '*': 1,
    '/': 1,
    '^': 2,
    '(': 3,
    ')': 3
  };

  for (var i = 0; i < token.length; i++) {
    if (int.tryParse(token[i]) != null) {
      postfix.add(token[i]);
    }

    if (operations.contains(token[i])) {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          procedence[token[i]]! < procedence[token.last]!) {
        postfix.add(operators.removeLast());
      }
      operators.add(token[i]);
    }

    if (token[i] == '(') {
      operators.add(token[i]);
    }

    if (token[i] == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

num evaluatePostfix(List<dynamic> token) {
  List<dynamic> values = [];
  num right, left, sum = 0;
  for (var i = 0; i < token.length; i++) {
    if (num.tryParse(token[i]) is num) {
      values.add(num.parse(token[i]));
    } else {
      right = values.removeLast();
      left = values.removeLast();
      switch (token[i]) {
        case '+':
          sum = left + right;
          break;
        case '-':
          sum = left - right;
          break;
        case '*':
          sum = left * right;
          break;
        case '/':
          sum = (left / right);
          break;
        case '^':
          sum = pow(left, right);
          break;
        default:
      }
      values.add(sum);
    }
  }
  return values.first;
}

import 'dart:io';
import '../lib/for_test.dart' as testLib;

void main() {
  String x = stdin.readLineSync()!;
  List<String> token = testLib.tokenizerString(x);
  List<dynamic> postfix = testLib.infixToPostfix(token);
  num result = testLib.evaluatePostfix(postfix);
  print("Tokenizer: $token");
  print("Convert infix to postfix: $postfix");
  print("Evaluate Postfix: $result");
}
